﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trESTController : ControllerBase
    {
        private readonly pgcontext _context;

        public trESTController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trEST
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trESTModel>>> GettrESTModel()
        {
            return await _context.trEST.ToListAsync();
        }

        //// GET: api/trEST/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trESTModel>> GettrESTModel(int? id)
        //{
        //    var trESTModel = await _context.trESTModel.FindAsync(id);

        //    if (trESTModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trESTModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrESTModel(string HN)
        {
            var ds = (from a in _context.trEST
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        [HttpGet("{HN}/{EN}")]

        public IActionResult GettrESTModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trEST
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trEST
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }


        // PUT: api/trEST/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrESTModel(int? id, trESTModel trESTModel)
        {
            if (id != trESTModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trESTModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trESTModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trEST
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trESTModel>> PosttrESTModel(trESTModel trESTModel)
        {
            _context.trEST.Add(trESTModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrESTModel", new { id = trESTModel.UID }, trESTModel);
        }

        // DELETE: api/trEST/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trESTModel>> DeletetrESTModel(int? id)
        {
            var trESTModel = await _context.trEST.FindAsync(id);
            if (trESTModel == null)
            {
                return NotFound();
            }

            _context.trEST.Remove(trESTModel);
            await _context.SaveChangesAsync();

            return trESTModel;
        }

        private bool trESTModelExists(int? id)
        {
            return _context.trEST.Any(e => e.UID == id);
        }
    }
}
