﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VW_LabController : ControllerBase
    {
        private readonly pgcontext _context;

        public VW_LabController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/VW_Lab
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VW_LabModel>>> GetVW_LabModel()
        {
            return await _context.VW_Lab.ToListAsync();
        }

        //// GET: api/VW_Lab/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<VW_LabModel>> GetVW_LabModel(int? id)
        //{
        //    var vW_LabModel = await _context.VW_LabModel.FindAsync(id);

        //    if (vW_LabModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return vW_LabModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult GetvW_LabModel(string HN)
        {
            var ds = (from a in _context.VW_Lab
                          //join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where a.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/VW_Lab/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVW_LabModel(int? id, VW_LabModel vW_LabModel)
        {
            if (id != vW_LabModel.trPatientUID)
            {
                return BadRequest();
            }

            _context.Entry(vW_LabModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VW_LabModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VW_Lab
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<VW_LabModel>> PostVW_LabModel(VW_LabModel vW_LabModel)
        {
            _context.VW_Lab.Add(vW_LabModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVW_LabModel", new { id = vW_LabModel.trPatientUID }, vW_LabModel);
        }

        // DELETE: api/VW_Lab/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<VW_LabModel>> DeleteVW_LabModel(int? id)
        {
            var vW_LabModel = await _context.VW_Lab.FindAsync(id);
            if (vW_LabModel == null)
            {
                return NotFound();
            }

            _context.VW_Lab.Remove(vW_LabModel);
            await _context.SaveChangesAsync();

            return vW_LabModel;
        }

        private bool VW_LabModelExists(int? id)
        {
            return _context.VW_Lab.Any(e => e.trPatientUID == id);
        }
    }
}
