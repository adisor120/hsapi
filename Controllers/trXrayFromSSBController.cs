﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trXrayFromSSBController : ControllerBase
    {
        private readonly pgcontext _context;

        public trXrayFromSSBController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trXrayFromSSB
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trXrayFromSSBModel>>> GettrXrayFromSSBModel()
        {
            return await _context.trXrayFromSSB.ToListAsync();
        }

        //// GET: api/trXrayFromSSB/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trXrayFromSSBModel>> GettrXrayFromSSBModel(int? id)
        //{
        //    var trXrayFromSSBModel = await _context.trXrayFromSSBModel.FindAsync(id);

        //    if (trXrayFromSSBModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trXrayFromSSBModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult GettrXrayFromSSBModel(string HN)
        {
            var ds = (from a in _context.trXrayFromSSB
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        [HttpGet("{HN}/{EN}")]

        public IActionResult GettrXrayFromSSBModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trXrayFromSSB
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }


        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trXrayFromSSB
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }


        // PUT: api/trXrayFromSSB/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrXrayFromSSBModel(int? id, trXrayFromSSBModel trXrayFromSSBModel)
        {
            if (id != trXrayFromSSBModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trXrayFromSSBModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trXrayFromSSBModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trXrayFromSSB
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trXrayFromSSBModel>> PosttrXrayFromSSBModel(trXrayFromSSBModel trXrayFromSSBModel)
        {
            _context.trXrayFromSSB.Add(trXrayFromSSBModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrXrayFromSSBModel", new { id = trXrayFromSSBModel.UID }, trXrayFromSSBModel);
        }

        // DELETE: api/trXrayFromSSB/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trXrayFromSSBModel>> DeletetrXrayFromSSBModel(int? id)
        {
            var trXrayFromSSBModel = await _context.trXrayFromSSB.FindAsync(id);
            if (trXrayFromSSBModel == null)
            {
                return NotFound();
            }

            _context.trXrayFromSSB.Remove(trXrayFromSSBModel);
            await _context.SaveChangesAsync();

            return trXrayFromSSBModel;
        }

        private bool trXrayFromSSBModelExists(int? id)
        {
            return _context.trXrayFromSSB.Any(e => e.UID == id);
        }
    }
}
