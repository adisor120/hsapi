﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class vw_HNCHKUP_DETAILController : ControllerBase
    {
        private readonly pgcontext _context;

        public vw_HNCHKUP_DETAILController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/vw_HNCHKUP_DETAIL
        [HttpGet]
        public async Task<ActionResult<IEnumerable<vw_HNCHKUP_DETAILModel>>> Getvw_HNCHKUP_DETAILModel()
        {
            return await _context.vw_HNCHKUP_DETAIL.ToListAsync();
        }

        //// GET: api/vw_HNCHKUP_DETAIL/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<vw_HNCHKUP_DETAILModel>> Getvw_HNCHKUP_DETAILModel(int? id)
        //{
        //    var vw_HNCHKUP_DETAILModel = await _context.vw_HNCHKUP_DETAILModel.FindAsync(id);

        //    if (vw_HNCHKUP_DETAILModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return vw_HNCHKUP_DETAILModel;
        //}

        [HttpGet("codechkup")]
        public IActionResult GetModel()
        {
            var ds = (from a in _context.vw_HNCHKUP_DETAIL
                          //join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                          //where p.ARCode == arcode &&
                          //p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                          //p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)
                      select new
                      {
                          a.codechkup,
                          a.namechkup
                      }).Distinct();

            //select a.codechkup).Distinct();
            return Ok(ds.ToArray());
        }


        // PUT: api/vw_HNCHKUP_DETAIL/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> Putvw_HNCHKUP_DETAILModel(int? id, vw_HNCHKUP_DETAILModel vw_HNCHKUP_DETAILModel)
        {
            if (id != vw_HNCHKUP_DETAILModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(vw_HNCHKUP_DETAILModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!vw_HNCHKUP_DETAILModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/vw_HNCHKUP_DETAIL
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<vw_HNCHKUP_DETAILModel>> Postvw_HNCHKUP_DETAILModel(vw_HNCHKUP_DETAILModel vw_HNCHKUP_DETAILModel)
        {
            _context.vw_HNCHKUP_DETAIL.Add(vw_HNCHKUP_DETAILModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getvw_HNCHKUP_DETAILModel", new { id = vw_HNCHKUP_DETAILModel.UID }, vw_HNCHKUP_DETAILModel);
        }

        // DELETE: api/vw_HNCHKUP_DETAIL/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<vw_HNCHKUP_DETAILModel>> Deletevw_HNCHKUP_DETAILModel(int? id)
        {
            var vw_HNCHKUP_DETAILModel = await _context.vw_HNCHKUP_DETAIL.FindAsync(id);
            if (vw_HNCHKUP_DETAILModel == null)
            {
                return NotFound();
            }

            _context.vw_HNCHKUP_DETAIL.Remove(vw_HNCHKUP_DETAILModel);
            await _context.SaveChangesAsync();

            return vw_HNCHKUP_DETAILModel;
        }

        private bool vw_HNCHKUP_DETAILModelExists(int? id)
        {
            return _context.vw_HNCHKUP_DETAIL.Any(e => e.UID == id);
        }
    }
}
