﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trHNCHKUP_DETAILController : ControllerBase
    {
        private readonly pgcontext _context;

        public trHNCHKUP_DETAILController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trHNCHKUP_DETAIL
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trHNCHKUP_DETAILModel>>> GettrHNCHKUP_DETAILModel()
        {
            return await _context.trHNCHKUP_DETAIL.ToListAsync();
        }

        //// GET: api/trHNCHKUP_DETAIL/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trHNCHKUP_DETAILModel>> GettrHNCHKUP_DETAILModel(int? id)
        //{
        //    var trHNCHKUP_DETAILModel = await _context.trHNCHKUP_DETAILModel.FindAsync(id);

        //    if (trHNCHKUP_DETAILModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trHNCHKUP_DETAILModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrHNCHKUP_DETAILModel(string HN)
        {
            var ds = (from a in _context.trHNCHKUP_DETAIL
                      //join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where a.HN == HN 
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        //[HttpGet("{HN}/{EN}")]

        //public IActionResult GettrHNCHKUP_DETAILModel_HN_And_EN(string HN, string EN)
        //{
        //    var ds = (from a in _context.trHNCHKUP_DETAIL
        //              join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
        //              where p.HN == HN && p.EN == EN
        //              select a).ToList();
        //    return Ok(ds.ToArray());
        //}

        //[HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        //public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        //{
        //    var ds = (from a in _context.trHNCHKUP_DETAIL
        //              join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
        //              where p.ARCode == arcode &&
        //              p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
        //              p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

        //              select a).ToList();
        //    return Ok(ds.ToArray());
        //}

        // PUT: api/trHNCHKUP_DETAIL/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrHNCHKUP_DETAILModel(int? id, trHNCHKUP_DETAILModel trHNCHKUP_DETAILModel)
        {
            if (id != trHNCHKUP_DETAILModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trHNCHKUP_DETAILModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trHNCHKUP_DETAILModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trHNCHKUP_DETAIL
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trHNCHKUP_DETAILModel>> PosttrHNCHKUP_DETAILModel(trHNCHKUP_DETAILModel trHNCHKUP_DETAILModel)
        {
            _context.trHNCHKUP_DETAIL.Add(trHNCHKUP_DETAILModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrHNCHKUP_DETAILModel", new { id = trHNCHKUP_DETAILModel.UID }, trHNCHKUP_DETAILModel);
        }

        // DELETE: api/trHNCHKUP_DETAIL/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trHNCHKUP_DETAILModel>> DeletetrHNCHKUP_DETAILModel(int? id)
        {
            var trHNCHKUP_DETAILModel = await _context.trHNCHKUP_DETAIL.FindAsync(id);
            if (trHNCHKUP_DETAILModel == null)
            {
                return NotFound();
            }

            _context.trHNCHKUP_DETAIL.Remove(trHNCHKUP_DETAILModel);
            await _context.SaveChangesAsync();

            return trHNCHKUP_DETAILModel;
        }

        private bool trHNCHKUP_DETAILModelExists(int? id)
        {
            return _context.trHNCHKUP_DETAIL.Any(e => e.UID == id);
        }
    }
}
