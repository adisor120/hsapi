﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trDentalController : ControllerBase
    {
        private readonly pgcontext _context;

        public trDentalController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trDental
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trDentalModel>>> GettrDentalModel()
        {
            return await _context.trDental.ToListAsync();
        }

        //// GET: api/trDental/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trDentalModel>> GettrDentalModel(int? id)
        //{
        //    var trDentalModel = await _context.trDentalModel.FindAsync(id);

        //    if (trDentalModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trDentalModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrDentalModel(string HN)
        {
            var ds = (from a in _context.trDental
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        [HttpGet("{HN}/{EN}")]

        public IActionResult GettrDentalModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trDental
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }


        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trDental
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }


        // PUT: api/trDental/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrDentalModel(int? id, trDentalModel trDentalModel)
        {
            if (id != trDentalModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trDentalModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trDentalModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trDental
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trDentalModel>> PosttrDentalModel(trDentalModel trDentalModel)
        {
            _context.trDental.Add(trDentalModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrDentalModel", new { id = trDentalModel.UID }, trDentalModel);
        }

        // DELETE: api/trDental/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trDentalModel>> DeletetrDentalModel(int? id)
        {
            var trDentalModel = await _context.trDental.FindAsync(id);
            if (trDentalModel == null)
            {
                return NotFound();
            }

            _context.trDental.Remove(trDentalModel);
            await _context.SaveChangesAsync();

            return trDentalModel;
        }

        private bool trDentalModelExists(int? id)
        {
            return _context.trDental.Any(e => e.UID == id);
        }
    }
}
