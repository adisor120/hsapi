﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trFlexibilityTestingModelsController : ControllerBase
    {
        private readonly pgcontext _context;

        public trFlexibilityTestingModelsController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trFlexibilityTestingModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trFlexibilityTestingModel>>> GettrFlexibilityTestingModel()
        {
            return await _context.trFlexibilityTesting.ToListAsync();
        }

        //// GET: api/trFlexibilityTestingModels/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trFlexibilityTestingModel>> GettrFlexibilityTestingModel(int? id)
        //{
        //    var trFlexibilityTestingModel = await _context.trFlexibilityTesting.FindAsync(id);

        //    if (trFlexibilityTestingModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trFlexibilityTestingModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrFlexibilityTestingModel(string HN)
        {
            var ds = (from a in _context.trFlexibilityTesting
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        [HttpGet("{HN}/{EN}")]

        public IActionResult GettrFlexibilityTestingModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trFlexibilityTesting
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trFlexibilityTesting
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/trFlexibilityTestingModels/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrFlexibilityTestingModel(int? id, trFlexibilityTestingModel trFlexibilityTestingModel)
        {
            if (id != trFlexibilityTestingModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trFlexibilityTestingModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trFlexibilityTestingModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trFlexibilityTestingModels
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trFlexibilityTestingModel>> PosttrFlexibilityTestingModel(trFlexibilityTestingModel trFlexibilityTestingModel)
        {
            _context.trFlexibilityTesting.Add(trFlexibilityTestingModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrFlexibilityTestingModel", new { id = trFlexibilityTestingModel.UID }, trFlexibilityTestingModel);
        }

        // DELETE: api/trFlexibilityTestingModels/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trFlexibilityTestingModel>> DeletetrFlexibilityTestingModel(int? id)
        {
            var trFlexibilityTestingModel = await _context.trFlexibilityTesting.FindAsync(id);
            if (trFlexibilityTestingModel == null)
            {
                return NotFound();
            }

            _context.trFlexibilityTesting.Remove(trFlexibilityTestingModel);
            await _context.SaveChangesAsync();

            return trFlexibilityTestingModel;
        }

        private bool trFlexibilityTestingModelExists(int? id)
        {
            return _context.trFlexibilityTesting.Any(e => e.UID == id);
        }
    }
}
