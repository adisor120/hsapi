﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class msusersController : ControllerBase
    {
        private readonly pgcontext _context;

        public msusersController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/msusers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<msuser>>> Getmsuser()
        {
            return await _context.msUser.ToListAsync();
        }

        // GET: api/msusers/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<msuser>> Getmsuser(int? id)
        //{
        //    var msuser = await _context.msuser.FindAsync(id);

        //    if (msuser == null)
        //    {
        //        return NotFound();
        //    }

        //    return msuser;
        //}
        [HttpGet("{user}/{pass}")]

        public IActionResult GettrLabModel_HN_And_EN(string user, string pass)
        {
            var ds = (from a in _context.msUser
                      where a.Username == user && a.Password == Encrypt(pass)
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpPost("{findmsuser}")]
        public async Task<ActionResult<msuser>> findmsuser(msuser msuser)
        {
            var ds = (from a in _context.msUser
                      where a.Username == msuser.Username && a.Password == Encrypt(msuser.Password)
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/msusers/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> Putmsuser(int? id, msuser msuser)
        {
            if (id != msuser.UID)
            {
                return BadRequest();
            }

            _context.Entry(msuser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!msuserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/msusers
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<msuser>> Postmsuser(msuser msuser)
        {
            _context.msUser.Add(msuser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getmsuser", new { id = msuser.UID }, msuser);
        }

        // DELETE: api/msusers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<msuser>> Deletemsuser(int? id)
        {
            var msuser = await _context.msUser.FindAsync(id);
            if (msuser == null)
            {
                return NotFound();
            }

            _context.msUser.Remove(msuser);
            await _context.SaveChangesAsync();

            return msuser;
        }

        private bool msuserExists(int? id)
        {
            return _context.msUser.Any(e => e.UID == id);
        }
        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
    }
}
