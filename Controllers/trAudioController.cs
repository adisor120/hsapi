﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trAudioController : ControllerBase
    {
        private readonly pgcontext _context;

        public trAudioController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trAudio
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trAudioModel>>> GettrAudioModel()
        {
            return await _context.trAudio.ToListAsync();
        }

        //// GET: api/trAudio/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trAudioModel>> GettrAudioModel(int? id)
        //{
        //    var trAudioModel = await _context.trAudioModel.FindAsync(id);

        //    if (trAudioModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trAudioModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult GettrAudioModel(string HN)
        {
            var ds = (from a in _context.trAudio
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        } 

        [HttpGet("{HN}/{EN}")]
        public IActionResult GettrAudioModel_HN_And_EN(string HN ,string EN)
        {
            var ds = (from a in _context.trAudio
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }


        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trAudio
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }


        // PUT: api/trAudio/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrAudioModel(int? id, trAudioModel trAudioModel)
        {
            if (id != trAudioModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trAudioModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trAudioModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trAudio
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trAudioModel>> PosttrAudioModel(trAudioModel trAudioModel)
        {
            _context.trAudio.Add(trAudioModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrAudioModel", new { id = trAudioModel.UID }, trAudioModel);
        }

        // DELETE: api/trAudio/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trAudioModel>> DeletetrAudioModel(int? id)
        {
            var trAudioModel = await _context.trAudio.FindAsync(id);
            if (trAudioModel == null)
            {
                return NotFound();
            }

            _context.trAudio.Remove(trAudioModel);
            await _context.SaveChangesAsync();

            return trAudioModel;
        }

        private bool trAudioModelExists(int? id)
        {
            return _context.trAudio.Any(e => e.UID == id);
        }
    }
}
