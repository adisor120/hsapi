﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class tr_patient_checkup_listModelController : ControllerBase
    {
        private readonly pgcontext _context;

        public tr_patient_checkup_listModelController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/tr_patient_checkup_listModel
        [HttpGet]
        public async Task<ActionResult<IEnumerable<tr_patient_checkup_listModel>>> Gettr_patient_checkup_listModel()
        {
            return await _context.tr_patient_checkup_list.ToListAsync();
        }

        // GET: api/tr_patient_checkup_listModel/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<tr_patient_checkup_listModel>> Gettr_patient_checkup_listModel(int? id)
        //{
        //    var tr_patient_checkup_listModel = await _context.tr_patient_checkup_list.FindAsync(id);

        //    if (tr_patient_checkup_listModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return tr_patient_checkup_listModel;
        //}

        [HttpGet("{checkupdate}&&{checkupdateto}")]
        public IActionResult GettrPatientModel(string checkupdate, string checkupdateto)
        {

            var ds = (from a in _context.vw_patient_checkup_list

                      where a.checkupdate >= Convert.ToDateTime(checkupdate).AddYears(+543)
                      && a.checkupdate <= Convert.ToDateTime(checkupdateto).AddYears(+543)
                      select a).ToList();
            if (ds == null)
            {
                return NotFound();
            }

            return Ok(ds);
        }
        [HttpGet("{HN}")]
        public IActionResult GettrPatientModel(string HN)
        {

            var ds = (from a in _context.vw_patient_checkup_list
                      where a.hn== HN
                      select a).ToList();
            if (ds == null)
            {
                return NotFound();
            }

            return Ok(ds);
        }
        //public IActionResult Gettr_patient_checkup_listModel(string checkupdate, string checkupdateto)
        //{

        //    string str = "";
        //    var CHL = (from a in _context.tr_patient_checkup_list
        //              where a.checkupdate >= Convert.ToDateTime(checkupdate).AddYears(+543) 
        //              && a.checkupdate <= Convert.ToDateTime(checkupdateto).AddYears(+543)
        //              select a).ToList();// new { result = a.result };
        //                                 //foreach (var item in CHL)
        //                                 //{
        //                                 //    //DataRow dr = dt.NewRow();
        //                                 //    str += item.result.ToString();
        //                                 //    //dt.Rows.Add(dr);

        //    //string Json = DataTableToJSONWithStringBuilder(dt);
        //    //str = str.Remove(str.Length - 1, 1) + ",";
        //    if (CHL == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(CHL);
        //}
        //public IActionResult GetmsCompanyModel1(string? ARName)
        //{
        //    var ds = (from c in _context.msCompany
        //                  //where c.SSB_ARCode == ARName
        //              select c).ToList();

        //    return Ok(ds);


        //        // PUT: api/tr_patient_checkup_listModel/5
        //        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        //        // more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPut("{id}")]
        public async Task<IActionResult> Puttr_patient_checkup_listModel(int? id, tr_patient_checkup_listModel tr_patient_checkup_listModel)
        {
            if (id != tr_patient_checkup_listModel.uid)
            {
                return BadRequest();
            }

            _context.Entry(tr_patient_checkup_listModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tr_patient_checkup_listModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/tr_patient_checkup_listModel
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<tr_patient_checkup_listModel>> Posttr_patient_checkup_listModel(tr_patient_checkup_listModel tr_patient_checkup_listModel)
        {
            _context.tr_patient_checkup_list.Add(tr_patient_checkup_listModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Gettr_patient_checkup_listModel", new { id = tr_patient_checkup_listModel.uid }, tr_patient_checkup_listModel);
        }

        // DELETE: api/tr_patient_checkup_listModel/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<tr_patient_checkup_listModel>> Deletetr_patient_checkup_listModel(int? id)
        {
            var tr_patient_checkup_listModel = await _context.tr_patient_checkup_list.FindAsync(id);
            if (tr_patient_checkup_listModel == null)
            {
                return NotFound();
            }

            _context.tr_patient_checkup_list.Remove(tr_patient_checkup_listModel);
            await _context.SaveChangesAsync();

            return tr_patient_checkup_listModel;
        }

        private bool tr_patient_checkup_listModelExists(int? id)
        {
            return _context.tr_patient_checkup_list.Any(e => e.uid == id);
        }
    }
}
