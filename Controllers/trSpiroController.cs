﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trSpiroController : ControllerBase
    {
        private readonly pgcontext _context;

        public trSpiroController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trSpiro
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trSpiroModel>>> GettrSpiroModel()
        {
            return await _context.trSpiro.ToListAsync();
        }

        //// GET: api/trSpiro/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trSpiroModel>> GettrSpiroModel(int? id)
        //{
        //    var trSpiroModel = await _context.trSpiroModel.FindAsync(id);

        //    if (trSpiroModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trSpiroModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult GettrSpiroModel(string HN)
        {
            var ds = (from a in _context.trSpiro
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("{HN}/{EN}")]
        public IActionResult GettrSpiroModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trSpiro
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trSpiro
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }



        // PUT: api/trSpiro/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrSpiroModel(int? id, trSpiroModel trSpiroModel)
        {
            if (id != trSpiroModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trSpiroModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trSpiroModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trSpiro
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trSpiroModel>> PosttrSpiroModel(trSpiroModel trSpiroModel)
        {
            _context.trSpiro.Add(trSpiroModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrSpiroModel", new { id = trSpiroModel.UID }, trSpiroModel);
        }

        // DELETE: api/trSpiro/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trSpiroModel>> DeletetrSpiroModel(int? id)
        {
            var trSpiroModel = await _context.trSpiro.FindAsync(id);
            if (trSpiroModel == null)
            {
                return NotFound();
            }

            _context.trSpiro.Remove(trSpiroModel);
            await _context.SaveChangesAsync();

            return trSpiroModel;
        }

        private bool trSpiroModelExists(int? id)
        {
            return _context.trSpiro.Any(e => e.UID == id);
        }
    }
}
