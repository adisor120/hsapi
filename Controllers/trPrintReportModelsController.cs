﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trPrintReportModelsController : ControllerBase
    {
        private readonly pgcontext _context;

        public trPrintReportModelsController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trPrintReportModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trPrintReportModel>>> GettrPrintReportModel()
        {
            return await _context.trPrintReport.ToListAsync();
        }

        // GET: api/trPrintReportModels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<trPrintReportModel>> GettrPrintReportModel(int? id)
        {
            var trPrintReportModel = await _context.trPrintReport.FindAsync(id);

            if (trPrintReportModel == null)
            {
                return NotFound();
            }

            return trPrintReportModel;
        }

        // PUT: api/trPrintReportModels/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrPrintReportModel(int? id, trPrintReportModel trPrintReportModel)
        {
            if (id != trPrintReportModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trPrintReportModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trPrintReportModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trPrintReportModels
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trPrintReportModel>> PosttrPrintReportModel(trPrintReportModel trPrintReportModel)
        {
            _context.trPrintReport.Add(trPrintReportModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrPrintReportModel", new { id = trPrintReportModel.UID }, trPrintReportModel);
        }

        // DELETE: api/trPrintReportModels/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trPrintReportModel>> DeletetrPrintReportModel(int? id)
        {
            var trPrintReportModel = await _context.trPrintReport.FindAsync(id);
            if (trPrintReportModel == null)
            {
                return NotFound();
            }

            _context.trPrintReport.Remove(trPrintReportModel);
            await _context.SaveChangesAsync();

            return trPrintReportModel;
        }

        private bool trPrintReportModelExists(int? id)
        {
            return _context.trPrintReport.Any(e => e.UID == id);
        }
    }
}
