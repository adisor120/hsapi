﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trLabController : ControllerBase
    {
        private readonly pgcontext _context;

        public trLabController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trLab
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trLabModel>>> GettrLabModel()
        {
            return await _context.trLab.ToListAsync();
        }

        //// GET: api/trLab/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trLabModel>> GettrLabModel(int? id)
        //{
        //    var trLabModel = await _context.trLabModel.FindAsync(id);

        //    if (trLabModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trLabModel;
        //}
        [HttpGet("{HN}")]
        public IActionResult GettrLabModel(string HN)
        {
            var ds = (from a in _context.trLab
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("{HN}/{EN}")]
        public IActionResult GettrLabModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trLab
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trLab
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/trLab/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrLabModel(int? id, trLabModel trLabModel)
        {
            if (id != trLabModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trLabModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trLabModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trLab
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trLabModel>> PosttrLabModel(trLabModel trLabModel)
        {
            _context.trLab.Add(trLabModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrLabModel", new { id = trLabModel.UID }, trLabModel);
        }

        // DELETE: api/trLab/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trLabModel>> DeletetrLabModel(int? id)
        {
            var trLabModel = await _context.trLab.FindAsync(id);
            if (trLabModel == null)
            {
                return NotFound();
            }

            _context.trLab.Remove(trLabModel);
            await _context.SaveChangesAsync();

            return trLabModel;
        }

        private bool trLabModelExists(int? id)
        {
            return _context.trLab.Any(e => e.UID == id);
        }
    }
}
