﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class _temprptlabController : ControllerBase
    {
        private readonly pgcontext _context;

        public _temprptlabController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/_temprptlab
        [HttpGet]
        public async Task<ActionResult<IEnumerable<_temprptlabModel>>> Get_temprptlabModel()
        {
            return await _context._temprptlab.ToListAsync();
        }

        //// GET: api/_temprptlab/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<_temprptlabModel>> Get_temprptlabModel(string id)
        //{
        //    var _temprptlabModel = await _context._temprptlabModel.FindAsync(id);

        //    if (_temprptlabModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return _temprptlabModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult Get_temprptlabModel(string HN)
        {
            var ds = (from a in _context._temprptlab
                      //join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where a.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }


        // PUT: api/_temprptlab/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> Put_temprptlabModel(string id, _temprptlabModel _temprptlabModel)
        {
            if (id != _temprptlabModel.IPADDS)
            {
                return BadRequest();
            }

            _context.Entry(_temprptlabModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_temprptlabModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/_temprptlab
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<_temprptlabModel>> Post_temprptlabModel(_temprptlabModel _temprptlabModel)
        {
            _context._temprptlab.Add(_temprptlabModel);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (_temprptlabModelExists(_temprptlabModel.IPADDS))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("Get_temprptlabModel", new { id = _temprptlabModel.IPADDS }, _temprptlabModel);
        }

        // DELETE: api/_temprptlab/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<_temprptlabModel>> Delete_temprptlabModel(string id)
        {
            var _temprptlabModel = await _context._temprptlab.FindAsync(id);
            if (_temprptlabModel == null)
            {
                return NotFound();
            }

            _context._temprptlab.Remove(_temprptlabModel);
            await _context.SaveChangesAsync();

            return _temprptlabModel;
        }

        private bool _temprptlabModelExists(string id)
        {
            return _context._temprptlab.Any(e => e.IPADDS == id);
        }
    }
}
