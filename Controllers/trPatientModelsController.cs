﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trPatientModelsController : ControllerBase
    {
        private readonly pgcontext _context;

        public trPatientModelsController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trPatientModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<vw_patient_checkup_listModel>>> GettrPatientModel()
        {
            return await _context.vw_patient_checkup_list.ToListAsync();
        }

        [HttpGet("{checkupdate}&&{checkupdateto}")]
        public IActionResult GettrPatientModel(string checkupdate, string checkupdateto)
        {

            var ds = (from a in _context.vw_patient_checkup_list

                      where a.checkupdate >= Convert.ToDateTime(checkupdate).AddYears(+543)
                      && a.checkupdate <= Convert.ToDateTime(checkupdateto).AddYears(+543)
                      select a).ToList();

            return Ok(ds);
        }

        //// GET: api/trPatientModels/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trPatientModel>> GettrPatientModel(int? id)
        //{
        //    var trPatientModel = await _context.trPatient.FindAsync(id);

        //    if (trPatientModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trPatientModel;
        //}


        // PUT: api/trPatientModels/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrPatientModel(int? id, trPatientModel trPatientModel)
        {
            if (id != trPatientModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trPatientModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trPatientModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trPatientModels
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trPatientModel>> PosttrPatientModel(trPatientModel trPatientModel)
        {
            _context.trPatient.Add(trPatientModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrPatientModel", new { id = trPatientModel.UID }, trPatientModel);
        }

        // DELETE: api/trPatientModels/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trPatientModel>> DeletetrPatientModel(int? id)
        {
            var trPatientModel = await _context.trPatient.FindAsync(id);
            if (trPatientModel == null)
            {
                return NotFound();
            }

            _context.trPatient.Remove(trPatientModel);
            await _context.SaveChangesAsync();

            return trPatientModel;
        }

        private bool trPatientModelExists(int? id)
        {
            return _context.trPatient.Any(e => e.UID == id);
        }
    }
}
