﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class tr_bookreportbkController : ControllerBase
    {
        private readonly pgcontext _context;

        public tr_bookreportbkController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/tr_bookreportbk
        [HttpGet]
        public async Task<ActionResult<IEnumerable<tr_bookreportbkModel>>> Gettr_bookreportbkModel()
        {
            return await _context.tr_bookreportbk.ToListAsync();
        }

        //// GET: api/tr_bookreportbk/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<tr_bookreportbkModel>> Gettr_bookreportbkModel(string id)
        //{
        //    var tr_bookreportbkModel = await _context.tr_bookreportbkModel.FindAsync(id);

        //    if (tr_bookreportbkModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return tr_bookreportbkModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult Getr_bookreportbkModel(string HN)
        {
            var ds = (from a in _context.tr_bookreportbk
                          //join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where a.hn == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/tr_bookreportbk/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> Puttr_bookreportbkModel(string id, tr_bookreportbkModel tr_bookreportbkModel)
        {
            if (id != tr_bookreportbkModel.hn)
            {
                return BadRequest();
            }

            _context.Entry(tr_bookreportbkModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tr_bookreportbkModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/tr_bookreportbk
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<tr_bookreportbkModel>> Posttr_bookreportbkModel(tr_bookreportbkModel tr_bookreportbkModel)
        {
            _context.tr_bookreportbk.Add(tr_bookreportbkModel);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (tr_bookreportbkModelExists(tr_bookreportbkModel.hn))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("Gettr_bookreportbkModel", new { id = tr_bookreportbkModel.hn }, tr_bookreportbkModel);
        }

        // DELETE: api/tr_bookreportbk/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<tr_bookreportbkModel>> Deletetr_bookreportbkModel(string id)
        {
            var tr_bookreportbkModel = await _context.tr_bookreportbk.FindAsync(id);
            if (tr_bookreportbkModel == null)
            {
                return NotFound();
            }

            _context.tr_bookreportbk.Remove(tr_bookreportbkModel);
            await _context.SaveChangesAsync();

            return tr_bookreportbkModel;
        }

        private bool tr_bookreportbkModelExists(string id)
        {
            return _context.tr_bookreportbk.Any(e => e.hn == id);
        }
    }
}
