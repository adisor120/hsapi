﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trEchoController : ControllerBase
    {
        private readonly pgcontext _context;

        public trEchoController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trEcho
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trEchoModel>>> GettrEchoModel()
        {
            return await _context.trEcho.ToListAsync();
        }

        //// GET: api/trEcho/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trEchoModel>> GettrEchoModel(int? id)
        //{
        //    var trEchoModel = await _context.trEchoModel.FindAsync(id);

        //    if (trEchoModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trEchoModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrEchoModel(string HN)
        {
            var ds = (from a in _context.trEcho
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        [HttpGet("{HN}/{EN}")]

        public IActionResult GettrEchoModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trEcho
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trEcho
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }


        // PUT: api/trEcho/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrEchoModel(int? id, trEchoModel trEchoModel)
        {
            if (id != trEchoModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trEchoModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trEchoModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trEcho
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trEchoModel>> PosttrEchoModel(trEchoModel trEchoModel)
        {
            _context.trEcho.Add(trEchoModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrEchoModel", new { id = trEchoModel.UID }, trEchoModel);
        }

        // DELETE: api/trEcho/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trEchoModel>> DeletetrEchoModel(int? id)
        {
            var trEchoModel = await _context.trEcho.FindAsync(id);
            if (trEchoModel == null)
            {
                return NotFound();
            }

            _context.trEcho.Remove(trEchoModel);
            await _context.SaveChangesAsync();

            return trEchoModel;
        }

        private bool trEchoModelExists(int? id)
        {
            return _context.trEcho.Any(e => e.UID == id);
        }
    }
}
