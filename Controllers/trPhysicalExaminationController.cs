﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trPhysicalExaminationController : ControllerBase
    {
        private readonly pgcontext _context;

        public trPhysicalExaminationController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trPhysicalExamination
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trPhysicalExaminationModel>>> GettrPhysicalExaminationModel()
        {
            return await _context.trPhysicalExamination.ToListAsync();
        }

        //// GET: api/trPhysicalExamination/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trPhysicalExaminationModel>> GettrPhysicalExaminationModel(int? id)
        //{
        //    var trPhysicalExaminationModel = await _context.trPhysicalExaminationModel.FindAsync(id);

        //    if (trPhysicalExaminationModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trPhysicalExaminationModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult GettrPhysicalExaminationModel(string HN)
        {
            var ds = (from a in _context.trPhysicalExamination
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        [HttpGet("{HN}/{EN}")]

        public IActionResult GettrPhysicalExaminationModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trPhysicalExamination
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }


        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trPhysicalExamination
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }


        // PUT: api/trPhysicalExamination/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrPhysicalExaminationModel(int? id, trPhysicalExaminationModel trPhysicalExaminationModel)
        {
            if (id != trPhysicalExaminationModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trPhysicalExaminationModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trPhysicalExaminationModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trPhysicalExamination
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trPhysicalExaminationModel>> PosttrPhysicalExaminationModel(trPhysicalExaminationModel trPhysicalExaminationModel)
        {
            _context.trPhysicalExamination.Add(trPhysicalExaminationModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrPhysicalExaminationModel", new { id = trPhysicalExaminationModel.UID }, trPhysicalExaminationModel);
        }

        // DELETE: api/trPhysicalExamination/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trPhysicalExaminationModel>> DeletetrPhysicalExaminationModel(int? id)
        {
            var trPhysicalExaminationModel = await _context.trPhysicalExamination.FindAsync(id);
            if (trPhysicalExaminationModel == null)
            {
                return NotFound();
            }

            _context.trPhysicalExamination.Remove(trPhysicalExaminationModel);
            await _context.SaveChangesAsync();

            return trPhysicalExaminationModel;
        }

        private bool trPhysicalExaminationModelExists(int? id)
        {
            return _context.trPhysicalExamination.Any(e => e.UID == id);
        }
    }
}
