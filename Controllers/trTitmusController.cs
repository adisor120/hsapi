﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trTitmusController : ControllerBase
    {
        private readonly pgcontext _context;

        public trTitmusController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trTitmus
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trTitmusModel>>> GettrTitmusModel()
        {
            return await _context.trTitmus.ToListAsync();
        }

        //// GET: api/trTitmus/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trTitmusModel>> GettrTitmusModel(int? id)
        //{
        //    var trTitmusModel = await _context.trTitmusModel.FindAsync(id);

        //    if (trTitmusModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trTitmusModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrTitmusModel(string HN)
        {
            var ds = (from s in _context.trTitmus
                      join c in _context.trPatient on s.trPatientUID.ToString() equals c.UID.ToString()
                      where c.HN == HN
                      select s).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("{HN}/{EN}")]
        public IActionResult GettrTitmusModel(string HN,string EN)
        {
            var ds = (from s in _context.trTitmus
                      join c in _context.trPatient on s.trPatientUID.ToString() equals c.UID.ToString()
                      where c.HN == HN
                      && c.EN==EN
                      select s).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trTitmus
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/trTitmus/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrTitmusModel(int? id, trTitmusModel trTitmusModel)
        {
            if (id != trTitmusModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trTitmusModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trTitmusModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trTitmus
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trTitmusModel>> PosttrTitmusModel(trTitmusModel trTitmusModel)
        {
            _context.trTitmus.Add(trTitmusModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrTitmusModel", new { id = trTitmusModel.UID }, trTitmusModel);
        }

        // DELETE: api/trTitmus/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trTitmusModel>> DeletetrTitmusModel(int? id)
        {
            var trTitmusModel = await _context.trTitmus.FindAsync(id);
            if (trTitmusModel == null)
            {
                return NotFound();
            }

            _context.trTitmus.Remove(trTitmusModel);
            await _context.SaveChangesAsync();

            return trTitmusModel;
        }

        private bool trTitmusModelExists(int? id)
        {
            return _context.trTitmus.Any(e => e.UID == id);
        }
    }
}
