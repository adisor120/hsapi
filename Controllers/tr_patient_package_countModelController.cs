﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class tr_patient_package_countModelController : ControllerBase
    {
        private readonly pgcontext _context;

        public tr_patient_package_countModelController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/tr_patient_package_countModel
        [HttpGet]
        public async Task<ActionResult<IEnumerable<tr_patient_package_countModel>>> Gettr_patient_package_countModel()
        {
            return await _context.tr_patient_package_count.ToListAsync();
        }

        ////GET: api/tr_patient_package_countModel/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<tr_patient_package_countModel>> Gettr_patient_package_countModel(int? id)
        //{
        //    var tr_patient_package_countModel = await _context.tr_patient_package_count.FindAsync(id);

        //    if (tr_patient_package_countModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return tr_patient_package_countModel;
        //}

        [HttpGet("{checkupdate}&&{checkupdateto}")]
        public IActionResult Gettr_patient_package_countModel(string checkupdate, string checkupdateto)
        {

            string str = "";
            var CHL = (from a in _context.tr_patient_package_count
                           where a.checkupdate >= Convert.ToDateTime(checkupdate).AddYears(+543) && a.checkupdate <= Convert.ToDateTime(checkupdateto).AddYears(+543)
                       select a).ToList();// new { result = a.result };
                                          //foreach (var item in CHL)
                                          //{
                                          //    //DataRow dr = dt.NewRow();
                                          //    str += item.result.ToString();
                                          //    //dt.Rows.Add(dr);

            //string Json = DataTableToJSONWithStringBuilder(dt);
            //str = str.Remove(str.Length - 1, 1) + ",";
            if (CHL == null)
            {
                return NotFound();
            }
            return Ok(CHL);
        }
        // PUT: api/tr_patient_package_countModel/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> Puttr_patient_package_countModel(int? id, tr_patient_package_countModel tr_patient_package_countModel)
        {
            if (id != tr_patient_package_countModel.uid)
            {
                return BadRequest();
            }

            _context.Entry(tr_patient_package_countModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tr_patient_package_countModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/tr_patient_package_countModel
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<tr_patient_package_countModel>> Posttr_patient_package_countModel(tr_patient_package_countModel tr_patient_package_countModel)
        {
            _context.tr_patient_package_count.Add(tr_patient_package_countModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Gettr_patient_package_countModel", new { id = tr_patient_package_countModel.uid }, tr_patient_package_countModel);
        }

        // DELETE: api/tr_patient_package_countModel/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<tr_patient_package_countModel>> Deletetr_patient_package_countModel(int? id)
        {
            var tr_patient_package_countModel = await _context.tr_patient_package_count.FindAsync(id);
            if (tr_patient_package_countModel == null)
            {
                return NotFound();
            }

            _context.tr_patient_package_count.Remove(tr_patient_package_countModel);
            await _context.SaveChangesAsync();

            return tr_patient_package_countModel;
        }

        private bool tr_patient_package_countModelExists(int? id)
        {
            return _context.tr_patient_package_count.Any(e => e.uid == id);
        }
    }
}
