﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trVitalSignController : ControllerBase
    {
        private readonly pgcontext _context;

        public trVitalSignController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trVitalSign
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trVitalSignModel>>> GettrVitalSign()
        {
            return await _context.trVitalSign.ToListAsync();
        }

        //// GET: api/trVitalSign/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trVitalSignModel>> GettrVitalSignModel(int? id)
        //{
        //    var trVitalSignModel = await _context.trVitalSign.FindAsync(id);

        //    if (trVitalSignModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trVitalSignModel;
        //}


        // GET: api/trVitalSign/5
        [HttpGet("{HN}")]

        public IActionResult GettrVitalSignModel(string HN)
        {
            var ds = (from s in _context.trVitalSign
                      join c in _context.trPatient on s.ptPatientUID.ToString() equals c.UID.ToString()
                      where c.HN == HN
                      select s).ToList();
            return Ok(ds.ToArray());
        }




        // PUT: api/trVitalSign/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrVitalSignModel(int? id, trVitalSignModel trVitalSignModel)
        {
            if (id != trVitalSignModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trVitalSignModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trVitalSignModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trVitalSign
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trVitalSignModel>> PosttrVitalSignModel(trVitalSignModel trVitalSignModel)
        {
            _context.trVitalSign.Add(trVitalSignModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrVitalSignModel", new { id = trVitalSignModel.UID }, trVitalSignModel);
        }

        // DELETE: api/trVitalSign/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trVitalSignModel>> DeletetrVitalSignModel(int? id)
        {
            var trVitalSignModel = await _context.trVitalSign.FindAsync(id);
            if (trVitalSignModel == null)
            {
                return NotFound();
            }

            _context.trVitalSign.Remove(trVitalSignModel);
            await _context.SaveChangesAsync();

            return trVitalSignModel;
        }

        private bool trVitalSignModelExists(int? id)
        {
            return _context.trVitalSign.Any(e => e.UID == id);
        }
    }
}
