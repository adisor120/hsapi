﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trDentalNewModelsController : ControllerBase
    {
        private readonly pgcontext _context;

        public trDentalNewModelsController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trDentalNewModels
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trDentalNewModel>>> GettrDentalNewModel()
        {
            return await _context.trDentalNew.ToListAsync();
        }

        //// GET: api/trDentalNewModels/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trDentalNewModel>> GettrDentalNewModel(int? id)
        //{
        //    var trDentalNewModel = await _context.trDentalNewModel.FindAsync(id);

        //    if (trDentalNewModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trDentalNewModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrDentalNewModel(string HN)
        {
            var ds = (from a in _context.trDentalNew
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }
        [HttpGet("{HN}/{EN}")]

        public IActionResult GettrDentalNewModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trDentalNew
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trDentalNew
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }



        // PUT: api/trDentalNewModels/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrDentalNewModel(int? id, trDentalNewModel trDentalNewModel)
        {
            if (id != trDentalNewModel.uid)
            {
                return BadRequest();
            }

            _context.Entry(trDentalNewModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trDentalNewModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trDentalNewModels
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trDentalNewModel>> PosttrDentalNewModel(trDentalNewModel trDentalNewModel)
        {
            _context.trDentalNew.Add(trDentalNewModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrDentalNewModel", new { id = trDentalNewModel.uid }, trDentalNewModel);
        }

        // DELETE: api/trDentalNewModels/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trDentalNewModel>> DeletetrDentalNewModel(int? id)
        {
            var trDentalNewModel = await _context.trDentalNew.FindAsync(id);
            if (trDentalNewModel == null)
            {
                return NotFound();
            }

            _context.trDentalNew.Remove(trDentalNewModel);
            await _context.SaveChangesAsync();

            return trDentalNewModel;
        }

        private bool trDentalNewModelExists(int? id)
        {
            return _context.trDentalNew.Any(e => e.uid == id);
        }
    }
}
