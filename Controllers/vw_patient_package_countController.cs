﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class vw_patient_package_countController : ControllerBase
    {
        private readonly pgcontext _context;

        public vw_patient_package_countController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/vw_patient_package_count
        [HttpGet]
        public async Task<ActionResult<IEnumerable<vw_patient_package_count>>> Getvw_patient_package_count()
        {
            return await _context.vw_patient_package_count.ToListAsync();
        }

        //// GET: api/vw_patient_package_count/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<vw_patient_package_count>> Getvw_patient_package_count(DateTime id)
        //{
        //    var vw_patient_package_count = await _context.vw_patient_package_count.FindAsync(id);

        //    if (vw_patient_package_count == null)
        //    {
        //        return NotFound();
        //    }

        //    return vw_patient_package_count;
        //}

        [HttpGet("{id}")]
        public IActionResult Getvw_patient_package_count(DateTime id)
        {

            var ds = (from a in _context.vw_patient_package_count

                      where a.checkupdate == Convert.ToDateTime(id).AddYears(0)
                      //&& a.checkupdate <= Convert.ToDateTime(checkupdateto).AddYears(+543)
                      select new { checkupdate= a.checkupdate,
                          patientcount=a.patientcount
                      }).ToList();

            return Ok(ds.ToArray());
        }

        // PUT: api/vw_patient_package_count/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> Putvw_patient_package_count(DateTime id, vw_patient_package_count vw_patient_package_count)
        {
            if (id != vw_patient_package_count.checkupdate)
            {
                return BadRequest();
            }

            _context.Entry(vw_patient_package_count).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!vw_patient_package_countExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/vw_patient_package_count
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<vw_patient_package_count>> Postvw_patient_package_count(vw_patient_package_count vw_patient_package_count)
        {
            _context.vw_patient_package_count.Add(vw_patient_package_count);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (vw_patient_package_countExists(vw_patient_package_count.checkupdate))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("Getvw_patient_package_count", new { id = vw_patient_package_count.checkupdate }, vw_patient_package_count);
        }

        // DELETE: api/vw_patient_package_count/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<vw_patient_package_count>> Deletevw_patient_package_count(DateTime id)
        {
            var vw_patient_package_count = await _context.vw_patient_package_count.FindAsync(id);
            if (vw_patient_package_count == null)
            {
                return NotFound();
            }

            _context.vw_patient_package_count.Remove(vw_patient_package_count);
            await _context.SaveChangesAsync();

            return vw_patient_package_count;
        }

        private bool vw_patient_package_countExists(DateTime id)
        {
            return _context.vw_patient_package_count.Any(e => e.checkupdate == id);
        }
    }
}
