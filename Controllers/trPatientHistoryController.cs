﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trPatientHistoryController : ControllerBase
    {
        private readonly pgcontext _context;

        public trPatientHistoryController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trPatientHistory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trPatientHistoryModel>>> GettrPatientHistoryModel()
        {
            return await _context.trPatientHistory.ToListAsync();
        }

        //// GET: api/trPatientHistory/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trPatientHistoryModel>> GettrPatientHistoryModel(int? id)
        //{
        //    var trPatientHistoryModel = await _context.trPatientHistoryModel.FindAsync(id);

        //    if (trPatientHistoryModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trPatientHistoryModel;
        //}

        [HttpGet("{HN}")]

        public IActionResult GettrPatientHistoryModel(string HN)
        {
            var ds = (from s in _context.trPatientHistory
                      join c in _context.trPatient on s.trPatientUID.ToString() equals c.UID.ToString()
                      where c.HN == HN
                      select s).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/trPatientHistory/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrPatientHistoryModel(int? id, trPatientHistoryModel trPatientHistoryModel)
        {
            if (id != trPatientHistoryModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trPatientHistoryModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trPatientHistoryModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trPatientHistory
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trPatientHistoryModel>> PosttrPatientHistoryModel(trPatientHistoryModel trPatientHistoryModel)
        {
            _context.trPatientHistory.Add(trPatientHistoryModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrPatientHistoryModel", new { id = trPatientHistoryModel.UID }, trPatientHistoryModel);
        }

        // DELETE: api/trPatientHistory/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trPatientHistoryModel>> DeletetrPatientHistoryModel(int? id)
        {
            var trPatientHistoryModel = await _context.trPatientHistory.FindAsync(id);
            if (trPatientHistoryModel == null)
            {
                return NotFound();
            }

            _context.trPatientHistory.Remove(trPatientHistoryModel);
            await _context.SaveChangesAsync();

            return trPatientHistoryModel;
        }

        private bool trPatientHistoryModelExists(int? id)
        {
            return _context.trPatientHistory.Any(e => e.UID == id);
        }
    }
}
