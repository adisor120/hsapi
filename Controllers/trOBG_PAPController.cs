﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class trOBG_PAPController : ControllerBase
    {
        private readonly pgcontext _context;

        public trOBG_PAPController(pgcontext context)
        {
            _context = context;
        }

        // GET: api/trOBG_PAP
        [HttpGet]
        public async Task<ActionResult<IEnumerable<trOBG_PAPModel>>> GettrOBG_PAPModel()
        {
            return await _context.trOBG_PAP.ToListAsync();
        }

        //// GET: api/trOBG_PAP/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<trOBG_PAPModel>> GettrOBG_PAPModel(int? id)
        //{
        //    var trOBG_PAPModel = await _context.trOBG_PAPModel.FindAsync(id);

        //    if (trOBG_PAPModel == null)
        //    {
        //        return NotFound();
        //    }

        //    return trOBG_PAPModel;
        //}

        [HttpGet("{HN}")]
        public IActionResult GettrOBG_PAPModel(string HN)
        {
            var ds = (from a in _context.trOBG_PAP
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN
                      select a).ToList();
            return Ok(ds.ToArray());
        }

        [HttpGet("{HN}/{EN}")]
        public IActionResult GettrOBG_PAPModel_HN_And_EN(string HN, string EN)
        {
            var ds = (from a in _context.trOBG_PAP
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.HN == HN && p.EN == EN
                      select a).ToList();
            return Ok(ds.ToArray());
        }


        [HttpGet("payor/{arcode}&&{checkupdate}&&{checkupdateto}")]
        public IActionResult GetModel(string arcode, string checkupdate, string checkupdateto)
        {
            var ds = (from a in _context.trOBG_PAP
                      join p in _context.trPatient on a.trPatientUID.ToString() equals p.UID.ToString()
                      where p.ARCode == arcode &&
                      p.CheckupDate >= Convert.ToDateTime(checkupdate).AddYears(0) &&
                      p.CheckupDate <= Convert.ToDateTime(checkupdateto).AddYears(0)

                      select a).ToList();
            return Ok(ds.ToArray());
        }

        // PUT: api/trOBG_PAP/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PuttrOBG_PAPModel(int? id, trOBG_PAPModel trOBG_PAPModel)
        {
            if (id != trOBG_PAPModel.UID)
            {
                return BadRequest();
            }

            _context.Entry(trOBG_PAPModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!trOBG_PAPModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/trOBG_PAP
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<trOBG_PAPModel>> PosttrOBG_PAPModel(trOBG_PAPModel trOBG_PAPModel)
        {
            _context.trOBG_PAP.Add(trOBG_PAPModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GettrOBG_PAPModel", new { id = trOBG_PAPModel.UID }, trOBG_PAPModel);
        }

        // DELETE: api/trOBG_PAP/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<trOBG_PAPModel>> DeletetrOBG_PAPModel(int? id)
        {
            var trOBG_PAPModel = await _context.trOBG_PAP.FindAsync(id);
            if (trOBG_PAPModel == null)
            {
                return NotFound();
            }

            _context.trOBG_PAP.Remove(trOBG_PAPModel);
            await _context.SaveChangesAsync();

            return trOBG_PAPModel;
        }

        private bool trOBG_PAPModelExists(int? id)
        {
            return _context.trOBG_PAP.Any(e => e.UID == id);
        }
    }
}
