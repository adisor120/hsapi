﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trXrayFromSSBModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ResultDetail { get; set; }
        public DateTime? ResultDate { get; set; }
        public DateTime? cwhen { get; set; }
        public DateTime? mwhen { get; set; }
        public string userID { get; set; }
        public string NResultDetail { get; set; }
        public string HN { get; set; }
        public string xx { get; set; }
        public string yy { get; set; }
        public string HSeriesResultDetail { get; set; }
        public string NormalStatusText { get; set; }
        public int? NormalStatusNum { get; set; }
        public string XrayStatus { get; set; }
        public string statusflag { get; set; }
    }
}
