﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class tr_bookreportbkModel
    {
        [Key]
        public int? patuid { get; set; }
        public string hn { get; set; }
        public string other_detail { get; set; }
    }
}
