﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class _temprptlabModel
    {
        [Key]
        public int? uid { get; set; }
        public string IPADDS { get; set; }
        public string HN { get; set; }
        public int? Seq { get; set; }
        public string Code { get; set; }
        public string NormalRange { get; set; }
        public string Category { get; set; }
        public string Group { get; set; }
        public string Desc { get; set; }
        public string ResultY1 { get; set; }
        public string ResultY2 { get; set; }
        public string ResultY3 { get; set; }
        public string ResultY4 { get; set; }
        public string ResultY5 { get; set; }
        public string UOM { get; set; }
        public string ResultCmd { get; set; }
    }
}
