﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trOBG_PAPModel
    {
        [Key]
        public int? UID { get; set; }
        public string Normal_CellsWere { get; set; }
        public string Normal_Inflammation { get; set; }
        public string Abnormal_Cervical { get; set; }
        public string Abnormal_Malignant { get; set; }
        public string Inflammation_Detail { get; set; }
        public string Cervical_Detail { get; set; }
        public string Rec_Annual { get; set; }
        public string Rec_PleaseVisit { get; set; }
        public string Rec_PleaseConsult { get; set; }
        public string PleaseVisit_Months { get; set; }
        public string Rec_Other { get; set; }
        public string Other_Detail { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public int? trPatientUID { get; set; }
        public string TranslateTH { get; set; }
        public string TranslateEN { get; set; }
    }
}
