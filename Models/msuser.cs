﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class msuser
    {
        [Key]
        public int? UID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string StatusFlag { get; set; }

        public string PrenameTH { get; set; }
        public string PrenameEN { get; set; }
        public string ForenameTH { get; set; }
        public string ForenameEN { get; set; }
        public string SurnameTH { get; set; }
        public string SurnameEN { get; set; }
        public string LicenseNo { get; set; }
        public string USER_PACS { get; set; }
        public string PW_PACS { get; set; }
        public string AUTH_Admin { get; set; }
    }
}
