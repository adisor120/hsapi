﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trDentalNewModel
    {
        [Key]
        public int? uid { get; set; }
        public int? trPatientUID { get; set; }
        public string question1 { get; set; }
        public string question2 { get; set; }
        public string question2_detail { get; set; }
        public string question3 { get; set; }
        public string question3_detail { get; set; }
        public string question4 { get; set; }
        public string question4_detail { get; set; }
        public string question5 { get; set; }
        public string question5_detail { get; set; }
        public string question6 { get; set; }
        public string question6_detail { get; set; }
        public string question7 { get; set; }
        public string question7_detail { get; set; }
        public string question_result_normal { get; set; }
        public string question_result_abnormal { get; set; }
        public string question_result_abnormal_detail { get; set; }
        public string question_result_abnormal_month { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public string StatusFlag { get; set; }
        public string question_danger { get; set; }
        public string doctor { get; set; }
        public string recommend { get; set; }
    }
}
