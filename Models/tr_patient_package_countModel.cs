﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class tr_patient_package_countModel
    {
        [Key]
        public int? uid { get; set; }
        public string packagecode { get; set; }
        public string packagename { get; set; }
        public DateTime? checkupdate {get;set;}
        public string patientcount { get; set; }
        public string status { get; set; }

}
}
