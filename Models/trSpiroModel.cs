﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trSpiroModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string FVC { get; set; }
        public string FVCPred { get; set; }
        public string FVCPer { get; set; }
        public string FEV1 { get; set; }
        public string FEV1Pred { get; set; }
        public string FEV1Per { get; set; }
        public string FEV1VFC { get; set; }
        public string FEV1VFCPred { get; set; }
        public string FEV1VFCPer { get; set; }
        public string FEF2575 { get; set; }
        public string FEF2575Pred { get; set; }
        public string FEF2575Per { get; set; }
        public string FEF { get; set; }
        public string FEFPred { get; set; }
        public string FEFPer { get; set; }
        public string ResultStatus { get; set; }
        public string SpiroCriteria { get; set; }
        public string TranslateStatus { get; set; }
        public string SpiroDetail { get; set; }
        public string Language { get; set; }
        public string StatusFlag { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
    }
}
