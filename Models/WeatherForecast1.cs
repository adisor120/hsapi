﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class WeatherForecast1
    {
        public class RootObject
        {
            public string title { get; set; }
            public string location_type { get; set; }
            public int woeid { get; set; }
            public string latt_long { get; set; }
        }
    }
}
