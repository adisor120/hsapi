﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIHSeriesDB.Models;

namespace APIHSeriesDB.Models
{
    public class pgcontext : DbContext
    {
        public pgcontext(DbContextOptions<pgcontext> options) : base(options) //connect data
        {

        }
        public DbSet<APIHSeriesDB.Models.tr_patient_checkup_listModel> tr_patient_checkup_list { get; set; }
        public DbSet<APIHSeriesDB.Models.tr_patient_package_countModel> tr_patient_package_count { get; set; }
        public DbSet<APIHSeriesDB.Models.trPrintReportModel> trPrintReport { get; set; }
        public DbSet<APIHSeriesDB.Models.trPatientModel> trPatient { get; set; }
        public DbSet<APIHSeriesDB.Models.trVitalSignModel> trVitalSign { get; set; }
        public DbSet<APIHSeriesDB.Models.vw_patient_checkup_listModel> vw_patient_checkup_list { get; set; }
        public DbSet<APIHSeriesDB.Models.vw_patient_package_count> vw_patient_package_count { get; set; }
        public DbSet<APIHSeriesDB.Models.trPatientHistoryModel> trPatientHistory { get; set; }
        public DbSet<APIHSeriesDB.Models.trTitmusModel> trTitmus { get; set; }
        public DbSet<APIHSeriesDB.Models.trAudioModel> trAudio { get; set; }
        public DbSet<APIHSeriesDB.Models.trDentalModel> trDental { get; set; }
        public DbSet<APIHSeriesDB.Models.trDentalNewModel> trDentalNew { get; set; }
        public DbSet<APIHSeriesDB.Models.trFlexibilityTestingModel> trFlexibilityTesting { get; set; }
        public DbSet<APIHSeriesDB.Models.trEKGModel> trEKG { get; set; }
        public DbSet<APIHSeriesDB.Models.trESTModel> trEST { get; set; }
        public DbSet<APIHSeriesDB.Models.trEchoModel> trEcho { get; set; }
        public DbSet<APIHSeriesDB.Models.trHNCHKUP_DETAILModel> trHNCHKUP_DETAIL { get; set; }
        public DbSet<APIHSeriesDB.Models.trLabModel> trLab { get; set; }
        public DbSet<APIHSeriesDB.Models.trOBG_PAPModel> trOBG_PAP { get; set; }
        public DbSet<APIHSeriesDB.Models.trSpiroModel> trSpiro { get; set; }
        public DbSet<APIHSeriesDB.Models.trXrayFromSSBModel> trXrayFromSSB { get; set; }
        public DbSet<APIHSeriesDB.Models.trPhysicalExaminationModel> trPhysicalExamination { get; set; }
        public DbSet<APIHSeriesDB.Models._temprptlabModel> _temprptlab { get; set; }
        public DbSet<APIHSeriesDB.Models.VW_LabModel> VW_Lab { get; set; }
        public DbSet<APIHSeriesDB.Models.tr_bookreportbkModel> tr_bookreportbk { get; set; }
        public DbSet<APIHSeriesDB.Models.msuser> msUser { get; set; }
        public DbSet<APIHSeriesDB.Models.vw_HNCHKUP_DETAILModel> vw_HNCHKUP_DETAIL { get; set; }
    }

}
