﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class vw_HNCHKUP_DETAILModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string HN { get; set; }
        public string RequestNo { get; set; }
        public string codechkup { get; set; }
        public string namechkup { get; set; }
        public string FacilityName { get; set; }
        public string CxlByUserCode { get; set; }
        public DateTime? Chekupdate { get; set; }
        public string CheckupDateNum { get; set; }
        public string chkup_type { get; set; }
        public string FacilityMethodCode { get; set; }
    }
}
