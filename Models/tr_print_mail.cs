﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class tr_print_mail
    {
        [Key]
        public int? uid { get; set; }
        public string trpatientuid { get; set; }
        public string maildate { get; set; }
        public string statusflag { get; set; }
        public int? cuser { get; set; }
        public DateTime? cwhen { get; set; }
        public int? muser { get; set; }
        public DateTime? mwhen { get; set; }
    }
}
