﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trLabModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string HN { get; set; }
        public string EN { get; set; }
        public string ItemCode { get; set; }
        public string ItemDesc { get; set; }
        public string StdComment { get; set; }
        public string TestData { get; set; }
        public string TranslateResult { get; set; }
        public string LabRange { get; set; }
        public string NormalStatus { get; set; }
        public string UOM { get; set; }
        public DateTime? ResultDate { get; set; }
        public string StatusFlag { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public string Recommend { get; set; }
        public string LabCommentCode { get; set; }
        public string LabCommentName { get; set; }
        public string ConclusionResultHIS { get; set; }
        public int? ResultType { get; set; }
        public string ResultClassifiedName { get; set; }
        public string LABResultClassifiedType { get; set; }
        public string ChkRequestNo { get; set; }
        public DateTime? ColVisitDate { get; set; }
        public int? MainPatientUID { get; set; }
        public string ExtCode { get; set; }
        public string LabEditer { get; set; }
        public string CheckupNo { get; set; }
        public string Category { get; set; }
        public string ResultUID { get; set; }
        public string sequence { get; set; }
    }
}
