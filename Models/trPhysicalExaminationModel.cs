﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trPhysicalExaminationModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string chife_complain { get; set; }
        public string head { get; set; }
        public string eyes { get; set; }
        public string ears { get; set; }
        public string nose { get; set; }
        public string oralcav { get; set; }
        public string neck { get; set; }
        public string chest { get; set; }
        public string breasts { get; set; }
        public string lung { get; set; }
        public string heart { get; set; }
        public string rhythm { get; set; }
        public string sound { get; set; }
        public string abdomen { get; set; }
        public string gastrointestinal { get; set; }
        public string liver { get; set; }
        public string spleen { get; set; }
        public string hernia { get; set; }
        public string anus { get; set; }
        public string spine { get; set; }
        public string extremities { get; set; }
        public string skin { get; set; }
        public string lymph { get; set; }
        public string neuro { get; set; }
        public string diagnosis { get; set; }
        public string indication_for_admit { get; set; }
        public string conclusion { get; set; }
        public string recommendation { get; set; }
        public string medication { get; set; }
        public string followup { get; set; }
        public string userid { get; set; }
        public DateTime? followup_date { get; set; }
        public DateTime? cwhen { get; set; }
        public DateTime? mwhen { get; set; }
        public string Other { get; set; }
        public string NoPE { get; set; }
        public string PEResult { get; set; }
        public string conclusionEN { get; set; }
        public string Editconclusion { get; set; }
        public string OtherTest { get; set; }
        public string OtherTestEn { get; set; }
        public string recommendationEn { get; set; }
        public string medicationEn { get; set; }
        public string DoctorComplete { get; set; }
        public string Respiratory { get; set; }
        public string UrinReproductive { get; set; }
        public string Mental { get; set; }
        public string LockConclusion { get; set; }
        public string OtoscopicExam { get; set; }
        public string Thyroidgland { get; set; }
        public string Externalgenitalia { get; set; }
        public string Rectalexamination { get; set; }
        public string Locomotor { get; set; }
        public string Reflexes { get; set; }
        public string Vertebrae { get; set; }
        public string Additional { get; set; }
        public string OtherExam { get; set; }
        public string Fit_Not { get; set; }
        public string HIV { get; set; }
    }
}
