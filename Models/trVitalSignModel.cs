﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trVitalSignModel
    {
        [Key]
        public int?  UID  {get;set;}
    public int?  ptPatientUID  {get;set;}
public string  Height  {get;set;}
    public string  Weight  {get;set;}
    public string  BMI  {get;set;}
    public string  BMIDetail  {get;set;}
    public string  BPSys  {get;set;}
    public string  BPDias  {get;set;}
    public string  BPDetail  {get;set;}
    public string  PulseRate  {get;set;}
    public string  PulseDetail  {get;set;}
    public string  VisionRWOG  {get;set;}
    public string  VisionLWOG  {get;set;}
    public string  VisionRWG  {get;set;}
    public string  VisionLWG  {get;set;}
    public string  Glasses  {get;set;}
    public string  VisionDetail  {get;set;}
    public string  ColorBlind  {get;set;}
    public string  ColorBlindInfo  {get;set;}
    public string  PhysicalExam  {get;set;}
    public string  PhysicalExamInfo  {get;set;}
    public string  Abdomen  {get;set;}
    public string  Waist  {get;set;}
    public string  WaistDetail  {get;set;}
    public string  Hip  {get;set;}
    public string  Prenacy  {get;set;}
    public string  DoctorDiage  {get;set;}
    public string  DoctorDiageInfo  {get;set;}
    public string  EyeProb  {get;set;}
    public string  EyeProbInfo  {get;set;}
    public string  Language  {get;set;}
    public string  StatusFlag  {get;set;}
    public int?  CUser  {get;set;}
    public DateTime ? CWhen  {get;set;}
    public int?  MUser  {get;set;}
    public DateTime ? MWhen  {get;set;}
    public string  BodyTemperature  {get;set;}
    public string  RespiratoryRate  {get;set;}
    public string  ChestInspiration  {get;set;}
    public string  ChestExpiration  {get;set;}
    public string  Remarks  {get;set;}
    public string  Remark_Name  {get;set;}
    public string  Remark_Name_EN  {get;set;}
    public string  BPDetailCompair  {get;set;}
    public DateTime? remark_date { get; set; }
    }
}
