﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trTitmusModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string Cubic { get; set; }
        public string GC { get; set; }
        public string VARt { get; set; }
        public string VALt { get; set; }
        public string Color { get; set; }
        public string StereoDepth { get; set; }
        public string Vertical { get; set; }
        public string Horizontal { get; set; }
        public string VFRt { get; set; }
        public string VFLt { get; set; }
        public string VANRt { get; set; }
        public string VANLt { get; set; }
        public string CubicNear { get; set; }
        public string EyeProb { get; set; }
        public string EyeProbDetail { get; set; }
        public string VisionWOGDetail { get; set; }
        public string GCDetail { get; set; }
        public string ColorBlindDetail { get; set; }
        public string StereoDepthDetail { get; set; }
        public string VerticalDetail { get; set; }
        public string HorizontalDetail { get; set; }
        public string RHVFDetail { get; set; }
        public string LHVFDetail { get; set; }
        public string VADetail { get; set; }
        public string VANRDetail { get; set; }
        public string VANLDetail { get; set; }
        public string Language { get; set; }
        public string StatusFlag { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public string IOP_Right_Eye { get; set; }
        public string IOP_Left_Eye { get; set; }
        public string IOP_Detail { get; set; }
        public string AR_RightEye_SPH { get; set; }
        public string AR_RightEye_CYL { get; set; }
        public string AR_LeftEye_SPH { get; set; }
        public string AR_LeftEye_CYL { get; set; }
        public string AutoRefractDetail { get; set; }
        public string TitmusDetail { get; set; }
        public string AR_RightEye_Axis { get; set; }
        public string AR_LeftEye_Axis { get; set; }
        public string CHKTitmus { get; set; }
        public string SLRnormal { get; set; }
        public string SLLnormal { get; set; }
        public string SLnormalDetail { get; set; }
        public string Rcataract { get; set; }
        public string Lcataract { get; set; }
        public string Rpterygium { get; set; }
        public string Lpterygium { get; set; }
        public string Rwindposthumously { get; set; }
        public string Lwindposthumously { get; set; }
        public string RLimestoneeyelids { get; set; }
        public string LLimestoneeyelids { get; set; }
        public string RDryeye { get; set; }
        public string LDryeye { get; set; }
        public string RConjunctivitis { get; set; }
        public string LConjunctivitis { get; set; }
        public string SLDetail { get; set; }
        public string RVisuallyimpaired { get; set; }
        public string LVisuallyimpaired { get; set; }
        public string VisuallyimpairedDetail { get; set; }
        public string SLRother { get; set; }
        public string SLLother { get; set; }
        public string SLotherDetail { get; set; }
        public string ColorAbDetail { get; set; }
    }
}
