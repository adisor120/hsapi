﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trFlexibilityTestingModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string FlexibilityTestingNum { get; set; }
        public string FlexibilityTestingDetail { get; set; }
        public string FlexibilityTestingResult { get; set; }
        public string MuscularStrengthNum { get; set; }
        public string MuscularStrengthDetail { get; set; }
        public string MuscularStrengthResult { get; set; }
        public string Cuser { get; set; }
        public string Muser { get; set; }
        public DateTime? Cwher { get; set; }
        public DateTime? Mwher { get; set; }
        public string FlexibilityTestingDetailEN { get; set; }
        public string FlexibilityTestingResultEN { get; set; }
        public string MuscularStrengthDetailEN { get; set; }
        public string MuscularStrengthResultEN { get; set; }
    }
}
