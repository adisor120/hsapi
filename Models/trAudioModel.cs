﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trAudioModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string R250 { get; set; }
        public string R500 { get; set; }
        public string R750 { get; set; }
        public string R1000 { get; set; }
        public string R2000 { get; set; }
        public string R3000 { get; set; }
        public string R4000 { get; set; }
        public string R6000 { get; set; }
        public string R8000 { get; set; }
        public string L250 { get; set; }
        public string L500 { get; set; }
        public string L750 { get; set; }
        public string L1000 { get; set; }
        public string L2000 { get; set; }
        public string L3000 { get; set; }
        public string L4000 { get; set; }
        public string L6000 { get; set; }
        public string L8000 { get; set; }
        public string ResultStatus { get; set; }
        public string RightEarDetail { get; set; }
        public string LeftEarDetail { get; set; }
        public string RecomEarDetail { get; set; }
        public string Language { get; set; }
        public string StatusFlag { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public string average_r { get; set; }
        public string average_rr { get; set; }
        public string average_l { get; set; }
        public string average_ll { get; set; }
    }
}
