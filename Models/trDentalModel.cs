﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trDentalModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string DentalID { get; set; }
        public string DentalDetail { get; set; }
        public string Language { get; set; }
        public string StatusFlag { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public string DentalHealthStatus { get; set; }
        public string NeedFill { get; set; }
        public string NeedGetRootCanal { get; set; }
        public string NeedRefill { get; set; }
        public string NeedExtraction { get; set; }
        public string NeedGingival { get; set; }
        public string MissingTooth { get; set; }
        public string Filling { get; set; }
        public string FillingNo { get; set; }
        public string RootCanal { get; set; }
        public string RootCanalNo { get; set; }
        public string Extraction { get; set; }
        public string ExtractionNo { get; set; }
        public string Gingival { get; set; }
        public string Denture { get; set; }
        public string Implant { get; set; }
        public string SeenDoctor { get; set; }
        public string Other { get; set; }
        public string OtherDetail { get; set; }
        public string Dentist { get; set; }
        public string CHKDental { get; set; }
    }
}
