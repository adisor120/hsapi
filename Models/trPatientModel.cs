﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trPatientModel
    {
        [Key]
        public int? UID { get; set; }
        public string HN { get; set; }
        public string EN { get; set; }
        public string Prename { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string RegisType { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public DateTime? CheckupDate { get; set; }
        public string LabNo { get; set; }
        public int? NO { get; set; }
        public string ProChkList { get; set; }
        public string GroupBook { get; set; }
        public string Company { get; set; }
        public string EmpID { get; set; }
        public string Position { get; set; }
        public string Shift { get; set; }
        public string Job { get; set; }
        public string Dept { get; set; }
        public string Section { get; set; }
        public string Line { get; set; }
        public string Division { get; set; }
        public string BusUnit { get; set; }
        public string BusDiv { get; set; }
        public string Location { get; set; }
        public string Language { get; set; }
        public string StatusFlag { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public string Address { get; set; }
        public string CARDID { get; set; }
        public string ARCode { get; set; }
        public string RefNo { get; set; }
        public string RequestNo { get; set; }
        public string ResultStatus { get; set; }
        public string CHECKUPMETHOD { get; set; }
        public string ProgramDesc { get; set; }
        public string ARName { get; set; }
        public string CompanyCode { get; set; }
        public string UIDLanguage { get; set; }
        public string NationalityCode { get; set; }
        public string Nationality { get; set; }
        public string ChekRight { get; set; }
        public Boolean ChekStation { get; set; }
        public string RightData { get; set; }
        public int? CountCheckList { get; set; }
        public string CustomerDept { get; set; }
        public string CustomerPosition { get; set; }
        public string CustomerDeptname { get; set; }
        public string CustomerPositionname { get; set; }
        public string Prename_Eng { get; set; }
        public string Forename_Eng { get; set; }
        public string Surname_Eng { get; set; }
        public string PKCode { get; set; }
        public string VSSTS { get; set; }
        public string StatusLoaded { get; set; }
        public DateTime? DateRegisByLoad { get; set; }
        public int? HispatientUID { get; set; }
        public int? HispatientvisitUID { get; set; }
        public string HisVisitCareProvider { get; set; }
        public string EmpPinCode { get; set; }
        public string EmpAmphur { get; set; }
        public string EmpProvince { get; set; }
        public string BatchNo { get; set; }
        public string HisVisitCareProviderEnglishName { get; set; }
        public string PassportID { get; set; }
        public string AddressEN { get; set; }
        public string AddressNumber { get; set; }
        public string HisVisitCareProviderLicenseID { get; set; }
        public int? MergeToPatientUID { get; set; }
        public string AgeDetail { get; set; }
        public string AddressOther { get; set; }
        public string CheckupNo { get; set; }
    }
}
