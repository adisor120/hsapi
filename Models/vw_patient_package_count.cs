﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class vw_patient_package_count
    {
        [Key]
        public DateTime checkupdate { get; set; }
        public string patientcount { get; set; }
    }
}
