﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class tr_patient_checkup_listModel
    {
        [Key]
        public int? uid { get; set; }
        public DateTime? checkupdate { get; set; }
        public DateTime? cwhen { get; set; }
        public DateTime? mwhen { get; set; }
        public string status { get; set; }
        public string result { get; set; }
        public string hn { get; set; }
        public string en { get; set; }
        public string prename { get; set; }
        public string forename { get; set; }
        public string surname { get; set; }
        public string age { get; set; }
        public string sex { get; set; }
        public string location { get; set; }
        public string arcode { get; set; }
        public string programdesc { get; set; }
        public string arname { get; set; }
        public string packagecode { get; set; }
        public string packagename { get; set; }


    }
}
