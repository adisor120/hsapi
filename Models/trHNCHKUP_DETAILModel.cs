﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trHNCHKUP_DETAILModel
    {
        [Key]
        public int? UID { get; set; }
        public string trPatientUID { get; set; }
        public string HN { get; set; }
        public string RequestNo { get; set; }
        public string LabCode { get; set; }
        public string LabName { get; set; }
        public string XrayCode { get; set; }
        public string XrayName { get; set; }
        public string TreatmentCode { get; set; }
        public string TreatmentName { get; set; }
        public string FacilityName { get; set; }
        public string CxlByUserCode { get; set; }
        public DateTime? Chekupdate { get; set; }
        public string CheckupDateNum { get; set; }
        public string FacilityMethodCode { get; set; }
        public string MergePack { get; set; }
    }
}
