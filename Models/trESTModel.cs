﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trESTModel
    {
        [Key]
        public int? UID { get; set; }
        public int? trPatientUID { get; set; }
        public string ESTID { get; set; }
        public string ESTDetail { get; set; }
        public string Language { get; set; }
        public string StatusFlag { get; set; }
        public int? CUser { get; set; }
        public DateTime? CWhen { get; set; }
        public int? MUser { get; set; }
        public DateTime? MWhen { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
