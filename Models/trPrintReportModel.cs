﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSeriesDB.Models
{
    public class trPrintReportModel
    {

        [Key]
        public int? UID {get;set;}
        public string Report {get;set;}
        public DateTime? PrintDate {get;set;}
        public string StatusFlag {get;set;}
        public int? CUser {get;set;}
        public DateTime? CWhen {get;set;}
        public int? MUser {get;set;}
        public DateTime? MWhen {get;set;}
        public int? trPatientUID {get;set;}
        public DateTime? SendDate {get;set;}
    }
}
